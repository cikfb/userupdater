# UserUpdater

UserUpdater updates a .DAT file that contains information about what drives users are able to access.
In particular, it will append "f:\\" to the "wr:" and "rd:" lines for users that equal 250, 251, or 252. 
If "f:\\" is already contained then nothing will be appended to the line. 

## Getting Started

This is a gradle project. In order to create a jar simply go to the gradle menu and click Tasks/build/jar.
The jar that gets created will be located in the directory build/libs. 

## Deployment

Once you have the jar, you'll need to put the ADXSSHXH.DAT file in the same directory that the jar is located.
Then, type the following command in the terminal to run the jar:

```
java -jar updateusers.jar
```

After the jar is finished running, the ADXSSHXH.DAT will be fully updated, and a log file called "UserUpdater_Log.0.log"
will be created. This will detail all the lines that were updated and any errors that were encountered during runtime.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Kyle Back (cikfb)** - *Initial work*

## License

This project is licensed under Wakefern's License - see the [LICENSE.md](LICENSE.md) file for details