package com.wakefern;

import java.io.IOException;
import java.util.logging.*;

public class LOG_HANDLER {
    private static boolean isSET = false;
    protected Logger LOGGER = null;
    private Level level;
    private String log_name;
    private FileHandler fileHandler;
    private Handler consoleHandler;

    public LOG_HANDLER(String log_name) {
        this.level = Level.INFO;
        this.log_name = "DEFAULT";
        this.fileHandler = null;
        this.consoleHandler = null;
        if (!isSET) {
            isSET = true;
            LogManager.getLogManager().reset();
            System.out.println("LOG MANAGER RESET");
            System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$-7s] [%2$s] [%5$s]%n");
        }

        this.log_name = log_name;
        System.out.println("Set Log Name to " + log_name);
        this.LOGGER = Logger.getLogger(log_name);
        this.LOGGER.setLevel(this.level);
    }

    public LOG_HANDLER(String log_name, String output_directory) {
        this.level = Level.INFO;
        this.log_name = "DEFAULT";
        this.fileHandler = null;
        this.consoleHandler = null;
        if (!isSET) {
            isSET = true;
            LogManager.getLogManager().reset();
            System.out.println("LOG MANAGER RESET");
            System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$-7s] [%2$s] [%5$s]%n");
        }

        this.log_name = log_name;
        System.out.println("Set Log Name to " + log_name);
        this.LOGGER = Logger.getLogger(log_name);
        this.LOGGER.setLevel(this.level);
        this.addFileHandler(output_directory + log_name + ".%g.log", 10485760, 10, true, Level.ALL);
        this.addConsoleHandler(Level.WARNING);
    }

    public LOG_HANDLER(String log_name, String output_directory, boolean is4690) {
        this.level = Level.INFO;
        this.log_name = "DEFAULT";
        this.fileHandler = null;
        this.consoleHandler = null;
        if (!isSET) {
            isSET = true;
            LogManager.getLogManager().reset();
            System.out.println("LOG MANAGER RESET");
            System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$-7s] [%2$s] [%5$s]%n");
        }

        this.log_name = log_name;
        System.out.println("Set Log Name to " + log_name);
        this.LOGGER = Logger.getLogger(log_name);
        this.LOGGER.setLevel(this.level);
        this.addFileHandler(output_directory + "%g.log", 1048576, 10, true, Level.ALL);
        this.addConsoleHandler(Level.WARNING);
    }

    public FileHandler getFileHandler() {
        return this.fileHandler;
    }

    public Handler getConsoleHandler() {
        return this.consoleHandler;
    }

    public void setFileHandler(FileHandler theFileHandler) {
        this.LOGGER.addHandler(theFileHandler);
    }

    public void setFileHandler(Handler consoleHandler) {
        this.LOGGER.addHandler(consoleHandler);
    }

    public void addFileHandler(String file_format, int limit, int count, boolean append, Level level) {
        try {
            this.fileHandler = new FileHandler(file_format, limit, count, append);
            this.LOGGER.addHandler(this.fileHandler);
            this.fileHandler.setFormatter(new SimpleFormatter());
            this.fileHandler.setLevel(level);
        } catch (IOException var7) {
            var7.printStackTrace();
        }

    }

    public void addConsoleHandler(Level level) {
        this.consoleHandler = new ConsoleHandler();
        this.consoleHandler.setFormatter(new SimpleFormatter());
        this.consoleHandler.setLevel(level);
        this.LOGGER.addHandler(this.consoleHandler);
    }

    public void setLevel(Level the_level) {
        this.level = the_level;
        this.LOGGER.setLevel(this.level);
    }

    public void setLevel(String level) {
        byte var3 = -1;
        switch(level.hashCode()) {
            case -1852393868:
                if (level.equals("SEVERE")) {
                    var3 = 2;
                }
                break;
            case 2158010:
                if (level.equals("FINE")) {
                    var3 = 3;
                }
                break;
            case 2251950:
                if (level.equals("INFO")) {
                    var3 = 0;
                }
                break;
            case 66898392:
                if (level.equals("FINER")) {
                    var3 = 4;
                }
                break;
            case 1842428796:
                if (level.equals("WARNING")) {
                    var3 = 1;
                }
                break;
            case 2073850267:
                if (level.equals("FINEST")) {
                    var3 = 5;
                }
        }

        switch(var3) {
            case 0:
                this.level = Level.INFO;
                break;
            case 1:
                this.level = Level.WARNING;
                break;
            case 2:
                this.level = Level.SEVERE;
                break;
            case 3:
                this.level = Level.FINE;
                break;
            case 4:
                this.level = Level.FINER;
                break;
            case 5:
                this.level = Level.FINEST;
                break;
            default:
                this.level = Level.ALL;
        }

        this.LOGGER.setLevel(this.level);
    }

    public void removeAllHandlers() {
        Handler[] handlers = this.LOGGER.getHandlers();
        Handler[] var2 = handlers;
        int var3 = handlers.length;

        for(int var4 = 0; var4 < var3; ++var4) {
            Handler handler = var2[var4];
            this.LOGGER.removeHandler(handler);
        }

    }
}