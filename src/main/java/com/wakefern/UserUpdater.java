package com.wakefern;

import java.io.*;

/**
 * UserUpdater iterates through a .DAT file that contains information on what drives a user can access,
 * and appends "f:\" to those that belong to 250, 251, or 252 if it doesn't already exist.
 */
public class UserUpdater
{
    public static final LOG_HANDLER log_handler = new LOG_HANDLER("UserUpdater_Log", "");

    /**
     * Main method for UserUpdater.
     * @param args are the command-line arguments.
     */
    public static void main(String[] args)
    {
        log_handler.LOGGER.info("Running UserUpdater...");
        File oldFile = createNewFile(System.getProperty("user.dir") + "/ADXSSHXH.DAT");
        File newFile = createNewFile(System.getProperty("user.dir") + "/ADXSSHXH-new.DAT");

        try (
            BufferedReader br =
                new BufferedReader(new FileReader(oldFile));
            BufferedWriter bw =
                new BufferedWriter(new FileWriter(newFile));
        ) {
            // First line of the .DAT file
            String line = br.readLine();

            // Keeps track of the current userNum
            String userNum = null;

            // Iterates through .DAT file, and appends to new file
            while (line != null)
            {
                String newLine = null;
                line = line.trim();

                // Already editing a user
                if (userNum != null)
                {
                    // Done editing current user
                    if (line.equals(""))
                    {
                        userNum = null;
                        newLine = "";
                    }
                    // Append to "rd:" or "wr:" if needed
                    else if (needsUpdate(userNum, line))
                    {
                        newLine = line + " f:\\";
                        log_handler.LOGGER.info("Appending \"f:\\\" to " + line.substring(0, 3) + ", user " + userNum);
                    }
                    // No updates needed
                    else
                    {
                        newLine = line;
                    }
                }
                // Didn't encounter next user yet
                else
                {
                    String userStr = "user: ";
                    int index = line.indexOf(userStr);

                    // Get userNum if the current line contains it
                    if (index != -1)
                    {
                        userNum = line.substring(index + userStr.length());
                    }

                    newLine = line;
                }

                bw.write(newLine + "\n");
                line = br.readLine();
            }
        }
        catch (Exception e)
        {
            log_handler.LOGGER.warning(getStackTrace(e));
            e.printStackTrace();
        }

        // Replaces new file with old file
        oldFile.delete();
        newFile.renameTo(oldFile);
        log_handler.LOGGER.info("UserUpdater finished updates.");
    }

    /**
     * Determines if a given line in the .DAT file that begins with "rd:" or "wr:" needs "f:\" to be appended.
     * @param userNum is the number of the user that's currently being checked.
     * @param line is the current line in the .DAT file (It can only be a "rd:" or "wr:" line).
     * @return true if the userNum is 205, 251, or 252, and "f:\" isn't already on the current line.
     * Otherwise, return false.
     */
    private static boolean needsUpdate(String userNum, String line)
    {
        return (
            (!line.contains("f:\\")) &&
            (line.contains("rd:") || line.contains("wr:")) &&
            (userNum.equals("250") || userNum.equals("251") || userNum.equals("252"))
        );
    }

    /**
     * Creates a new file where the updated user information will be written.
     * @return the newly created File.
     */
    private static File createNewFile(String path)
    {
        // The file will be located in the directory where the jar is running
        File newFile = new File(path);

        try
        {
            // Creates the file if it doesn't already exist
            newFile.createNewFile();
        }
        catch (java.io.IOException e)
        {
            log_handler.LOGGER.warning(getStackTrace(e));
            e.printStackTrace();
        }

        return newFile;
    }

    /**
     * Converts an exception to a stack trace in string form.
     * @param e is the exception being converted.
     * @return the string form of the stack trace.
     */
    private static String getStackTrace(Exception e)
    {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        return sw.toString();
    }
}